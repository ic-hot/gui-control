package ic.gui.control.base


import ic.gui.control.env.GuiEnvironment


typealias BaseSetStateGuiController<State>
	= BaseSetStateGuiControllerWithEnv<State, GuiEnvironment>
;