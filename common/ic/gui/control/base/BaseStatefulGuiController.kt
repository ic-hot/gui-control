package ic.gui.control.base


import ic.gui.control.env.GuiEnvironment


typealias BaseStatefulGuiController<State>
	= BaseStatefulGuiControllerWithEnv<State, GuiEnvironment>
;
