package ic.gui.control.base


import ic.gui.control.env.GuiEnvironment


abstract class BaseGuiController : BaseStatefulGuiControllerWithEnv<Unit, GuiEnvironment>() {


	override fun snapshotState() = Unit


}