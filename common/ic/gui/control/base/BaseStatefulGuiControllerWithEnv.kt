package ic.gui.control.base


import ic.design.control.gen.BaseGenerativeStatefulControllerWithEnv
import ic.util.geo.Location

import ic.graphics.image.Image

import ic.gui.control.env.GuiEnvironment
import ic.gui.control.abstr.AbstractStatefulGuiControllerWithEnv
import ic.gui.control.ext.updateView
import ic.gui.view.View


abstract class BaseStatefulGuiControllerWithEnv<State, Environment: GuiEnvironment>
	: BaseGenerativeStatefulControllerWithEnv<View, State, Environment>()
	, AbstractStatefulGuiControllerWithEnv<State, Environment>
{


	protected abstract fun initView() : View


	protected open fun onImageSelected (image: Image) {}

	protected open fun onPermissionDenied() {}
	protected open fun onPermissionGranted() {}

	protected open fun onLocationUpdated (location: Location) {}

	protected open fun onRequestLocationFailed() {}

	protected open fun onKeyboardShown()  {}
	protected open fun onKeyboardHidden() {}


	override fun initSubject() = initView()


	override fun onOpenWithState (state: State) {
		updateView()
	}


	override fun notifyImageSelected (image: Image) = onImageSelected(image)

	override fun notifyPermissionDenied()  = onPermissionDenied()
	override fun notifyPermissionGranted() = onPermissionGranted()

	override fun notifyLocationUpdated (location: Location) = onLocationUpdated(location)

	override fun notifyRequestLocationFailed() = onRequestLocationFailed()

	override fun notifyKeyboardShown()  = onKeyboardShown()
	override fun notifyKeyboardHidden() = onKeyboardHidden()


}