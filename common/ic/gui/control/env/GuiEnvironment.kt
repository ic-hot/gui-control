package ic.gui.control.env


import ic.gui.scope.GuiScope


interface GuiEnvironment {

	val guiScope : GuiScope

}