package ic.gui.control.env


import ic.gui.scope.GuiScope


fun GuiEnvironment (

	guiScope : GuiScope

) : GuiEnvironment {

	return object : GuiEnvironment {

		override val guiScope get() = guiScope

	}

}