package ic.gui.control.abstr


import ic.gui.control.env.GuiEnvironment


typealias AbstractGuiController = AbstractStatefulGuiControllerWithEnv<Unit, GuiEnvironment>