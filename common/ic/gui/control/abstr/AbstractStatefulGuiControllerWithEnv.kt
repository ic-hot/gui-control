package ic.gui.control.abstr


import ic.design.control.gen.GenerativeStatefulControllerWithEnv
import ic.util.geo.Location

import ic.graphics.image.Image

import ic.gui.control.env.GuiEnvironment
import ic.gui.scope.proxy.ProxyGuiScope
import ic.gui.view.View


interface AbstractStatefulGuiControllerWithEnv<State, Environment: GuiEnvironment>
	: GenerativeStatefulControllerWithEnv<View, State, Environment>
	, ProxyGuiScope
{

	override val sourceGuiScope get() = environment.guiScope

	fun notifyImageSelected (image: Image)

	fun notifyPermissionDenied()
	fun notifyPermissionGranted()

	fun notifyLocationUpdated (location: Location)

	fun notifyRequestLocationFailed()

	fun notifyKeyboardShown()
	fun notifyKeyboardHidden()

}