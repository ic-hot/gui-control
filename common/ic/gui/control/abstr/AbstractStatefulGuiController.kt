package ic.gui.control.abstr


import ic.gui.control.env.GuiEnvironment


typealias AbstractStatefulGuiController<State>

	= AbstractStatefulGuiControllerWithEnv<State, GuiEnvironment>

;