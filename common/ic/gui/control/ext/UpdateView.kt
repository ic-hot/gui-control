package ic.gui.control.ext


import ic.gui.control.abstr.StatefulGuiControllerWithEnv
import ic.gui.view.ext.update


fun StatefulGuiControllerWithEnv<*, *>.updateView() {

	subject.update()

}