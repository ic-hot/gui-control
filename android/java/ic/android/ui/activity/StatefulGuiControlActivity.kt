@file:Suppress("DEPRECATION")


package ic.android.ui.activity


import android.graphics.Bitmap
import android.os.Bundle

import ic.util.geo.Location
import ic.android.ui.activity.decor.DecoratedActivity
import ic.android.util.bundle.ext.getAsBundleOrNull
import ic.android.util.bundle.ext.set
import ic.ifaces.pausable.Pausable
import ic.ifaces.stateful.ext.state

import ic.graphics.image.ImageFromAndroidBitmap

import ic.gui.scope.AndroidGuiScope
import ic.gui.control.env.GuiEnvironment
import ic.gui.control.abstr.AbstractStatefulGuiController
import ic.gui.control.ext.updateView


@Deprecated("Use ViewControllerActivity")
abstract class StatefulGuiControlActivity<State> : DecoratedActivity() {


	protected abstract fun initGuiController() : AbstractStatefulGuiController<State>

	protected abstract fun initState() : State

	protected abstract fun serializeStateToBundle (state: State) : Bundle

	protected abstract fun parseStateFromBundle (bundle: Bundle) : State


	private var guiController : AbstractStatefulGuiController<State>? = null


	override fun onCreate (stateBundle: Bundle?) {
		super.onCreate(stateBundle)
		val guiController = initGuiController()
		this.guiController = guiController
		val guiControllerStateBundle = stateBundle?.getAsBundleOrNull("viewControllerState")
		val state = (
			if (guiControllerStateBundle == null) {
				initState()
			} else {
				parseStateFromBundle(guiControllerStateBundle)
			}
		)
		val view = guiController.openWithState(
			environment = GuiEnvironment(
				guiScope = AndroidGuiScope(context = this)
			),
			state = state
		)
		setContentView(view as android.view.View)
	}


	override fun onResume() {
		super.onResume()
		val guiController = this.guiController
		if (guiController is Pausable) {
			guiController.resume()
		}
	}


	override fun onPause() {
		super.onPause()
		val guiController = this.guiController
		if (guiController is Pausable) {
			guiController.pause()
		}
	}


	override fun onPermissionDenied() {
		guiController!!.notifyPermissionDenied()
	}

	override fun onPermissionGranted() {
		guiController!!.notifyPermissionGranted()
	}

	override fun onLocationUpdated (location: Location) {
		guiController!!.notifyLocationUpdated(location)
	}

	override fun onRequestLocationFailed() {
		guiController!!.notifyRequestLocationFailed()
	}


	override fun saveStateToBundle (stateBundle: Bundle) {
		val guiController = this.guiController
		if (guiController != null) {
			stateBundle["viewControllerState"] = serializeStateToBundle(guiController.state)
		}
	}


	override fun onImageSelected (bitmap: Bitmap) {
		guiController!!.notifyImageSelected(
			ImageFromAndroidBitmap(bitmap)
		)
	}


	override fun onInsetsChanged() {
		guiController?.updateView()
	}


	override fun onKeyboardShown() {
		guiController?.updateView()
		guiController?.notifyKeyboardShown()
	}

	override fun onKeyboardHidden() {
		guiController?.updateView()
		guiController?.notifyKeyboardHidden()
	}


	override fun onDestroy() {
		super.onDestroy()
		guiController!!.close()
		guiController = null
	}


}