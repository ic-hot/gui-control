package ic.android.ui.activity


import android.os.Bundle


abstract class GuiControlActivity : StatefulGuiControlActivity<Unit>() {


	override fun initState() = Unit

	override fun parseStateFromBundle (bundle: Bundle) = Unit

	override fun serializeStateToBundle (state: Unit) = Bundle()


}